<?php
$first_name = $_POST['firstName'];
$last_name = $_POST['lastName'];
$age = $_POST['age'];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/style.css" media="screen">
    <title>Document</title>
</head>
<body>
    <h1>page 2</h1>
    <a href="index.php"><button>back to page 1</button></a>
    <div class="container">
        <div class="firstName">
            First Name: <?php echo $first_name;?>
        </div>
        <div class="lastName">
           Last Name:  <?php echo $last_name;?>
        </div>
        <div class="age">
           Age:  <?php echo $age;?>
        </div>
    </div>
</body>
</html>
