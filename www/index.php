<!DOCTYPE html>
<html lang="fr">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.min.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Questrial&display=swap" />
    <link rel="stylesheet" href="/css/style.css"/>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <title>Accueil</title>
</head>

<body>
  <!-- <a href="index2.php">go to page 2</a>
    <div class="col">
        <div class="row justify-content-between" id="logoConnexion">
          <div class="connexion" id="connect">
            <button type="button" class="btn btn-success btn-lg" onClick="window.location.href=''"><i class="fas fa-arrow-alt-circle-right"></i>&nbsp;Connexion</button>
          </div>
        </div>
    </div> -->
  <form method="POST" action="index2.php">
    <div class="form-group">
      <label for="exampleInputPassword1">First Name</label>
      <input type="text" class="form-control" id="exampleInputPassword1" placeholder="First Name" name="firstName">
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Last name</label>
      <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Last name" name="lastName">
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Age</label>
      <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Age" name="age">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
</body>

</html>

